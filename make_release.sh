#!/bin/sh
./json2schema.py
LATEST_TAG=$(git describe --abbrev=0)
LATEST_VERSION=$(echo "${LATEST_TAG}" | sed -e 's/v//')
NEW_VERSION=$((LATEST_VERSION+1))
if [ -z "${ZIP_NAME}" ]; then
    ZIP_FILE="todo.txt@bart.libert.gmail.com-v${NEW_VERSION}.zip"
else
    ZIP_FILE=${ZIP_NAME}
fi
rm "${ZIP_FILE}"
json -I -f metadata.json -E "this.version = ${NEW_VERSION}"
if [ -z "${NO_COMMIT}" ]; then
    git add metadata.json
    git commit -m "Update metadata"
    git tag -a "v${NEW_VERSION}"
fi
glib-compile-schemas schemas
zip "${ZIP_FILE}" -MM -T -@ < dist_files.lst
