const Class = imports.lang.Class;
const Formatter = imports.formatter;
const Helper = imports.unit.lib.utest_helpers;
const Unit = imports.third_party.gjsunit.gjsunit;
const Utils = imports.utils;

var suite = new Unit.Suite('Formatter');

const TestReplacementProvider = new Class({
    Name: 'TestReplacementProvider',
    numberOfTasks: 1,
    getNumberOfTasks: function() {
        return this.numberOfTasks;
    }
});

suite.setup = function() {
    suite.formatter = new Formatter.Formatter();
    suite.formatter.setLogger(Utils.getDefaultLogger());
};

suite.teardown = function() {
    suite.formatter = null;
};

suite.addTest('initialization', function() {
    Unit.assertNull(suite.formatter._format);
    Unit.assertEquals(suite.formatter._leftDelimiter, '%');
    Unit.assertEquals(suite.formatter._rightDelimiter, '');
    Unit.assertEquals(suite.formatter._tokenLength, 1);
    Unit.assertEquals(suite.formatter._externalParserLeftDelimiter, '|');
    Unit.assertEquals(suite.formatter._externalParserRightDelimiter, '|');
    Unit.assertNull(suite.formatter._externalParser);
    let formatterInit = new Formatter.Formatter('{tasks}');
    Unit.assertEquals(formatterInit._format, '{tasks}');
});

suite.addTest('set delimiters', function() {
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    Unit.assertEquals(suite.formatter._leftDelimiter, '{');
    Unit.assertEquals(suite.formatter._rightDelimiter, '}');
    suite.formatter.setLeftDelimiter();
    suite.formatter.setRightDelimiter();
    Unit.assertEquals(suite.formatter._leftDelimiter, '%');
    Unit.assertEquals(suite.formatter._rightDelimiter, '');
});

suite.addTest('set token length', function() {
    suite.formatter.setTokenLength(20);
    Unit.assertEquals(suite.formatter._tokenLength, 20);
    suite.formatter.setTokenLength(0);
    Unit.assertEquals(suite.formatter._tokenLength, 0);
});

suite.addTest('set format string', function() {
    suite.formatter.setFormatString('%s %d');
    Unit.assertEquals(suite.formatter._format, '%s %d');
    suite.formatter.setFormatString('{format}');
    Unit.assertEquals(suite.formatter._format, '{format}');
    suite.formatter.setFormatString();
    Unit.assertNull(suite.formatter._format);
});

suite.addTest('get string - no replacements defined', function() {
    suite.formatter.setFormatString('%s %d');
    Helper.assertThrows(function() {
        suite.formatter.getString();
    }, 'UndefinedTokenError');
});

suite.addTest('set replacements', function() {
    Unit.assertTrue(suite.formatter.setReplacement('s', 'string'));
    Unit.assertTrue(suite.formatter.setReplacement('d', 10));
    Unit.assertEquals(suite.formatter._replacements.s, 'string');
    Unit.assertEquals(suite.formatter._replacements.d, 10);
    Unit.assertTrue(suite.formatter.setReplacement());
    Unit.assertEquals(suite.formatter._replacements.s, 'string');
    Unit.assertTrue(suite.formatter.setReplacement('s'));
    Unit.assertUndefined(suite.formatter._replacements.s);
});

suite.addTest('set invalid replacements', function() {
    Unit.assertFalse(suite.formatter.setReplacement('^', 'string'));
    Unit.assertFalse(suite.formatter.setReplacement('d10', 10));
    Unit.assertFalse(suite.formatter.setReplacement('%', 10));
});

suite.addTest('get string - invalid combination of settings', function() {
    suite.formatter.setTokenLength(0);
    suite.formatter.setRightDelimiter('');
    Helper.assertThrows(function() {
        suite.formatter.getString();
    }, 'ConfigurationError');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setExternalParserLeftDelimiter('');
    suite.formatter.setExternalParserRightDelimiter('|');
    Helper.assertThrows(function() {
        suite.formatter.getString();
    }, 'ConfigurationError');
});

suite.addTest('get string with replacements - printf style replacements', function() {
    suite.formatter.setReplacement('s', 'string');
    suite.formatter.setReplacement('d', 10);
    suite.formatter.setFormatString('%s %d');
    Unit.assertEquals(suite.formatter.getString(), 'string 10');
    suite.formatter.setFormatString('the string is %s and %d is the decimal');
    Unit.assertEquals(suite.formatter.getString(), 'the string is string and 10 is the decimal');
});

suite.addTest('get string with replacements - bracket replacements', function() {
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setTokenLength(0);
    suite.formatter.setFormatString('{somestring} {somenumber} {somestring}');
    suite.formatter.setReplacement('somestring', 'this is a string');
    suite.formatter.setReplacement('somenumber', 43);
    Unit.assertEquals(suite.formatter.getString(), 'this is a string 43 this is a string');
});

suite.addTest('get string with replacements - bracket replacements from function', function() {
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setTokenLength(0);
    suite.formatter.setFormatString('number of tasks: {numberoftasks}');
    let replacer = new TestReplacementProvider();
    suite.formatter.setReplacement('numberoftasks', function() {
        return replacer.getNumberOfTasks();
    });
    replacer.numberOfTasks = 3;
    Unit.assertEquals(suite.formatter.getString(), 'number of tasks: 3');
    replacer.numberOfTasks = 4;
    Unit.assertEquals(suite.formatter.getString(), 'number of tasks: 4');
});

suite.addTest('get string with replacements - printf style replacements with left padding', function() {
    suite.formatter.setReplacement('s', 'string');
    suite.formatter.setReplacement('d', 10);
    suite.formatter.setFormatString('%8ls %4l0d %3ls');
    Unit.assertEquals(suite.formatter.getString(), '  string 0010 string');
    suite.formatter.setFormatString('the string is %6ls and %1l0d is the decimal');
    Unit.assertEquals(suite.formatter.getString(), 'the string is string and 10 is the decimal');
});

suite.addTest('get string with replacements - bracket replacements with left padding', function() {
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setTokenLength(0);
    suite.formatter.setFormatString('{20l:somestring} {3lx:somenumber} {5l0:somestring}');
    suite.formatter.setReplacement('somestring', 'this is a string');
    suite.formatter.setReplacement('somenumber', 43);
    Unit.assertEquals(suite.formatter.getString(), '    this is a string x43 this is a string');
});

suite.addTest('get string with replacements - bracket replacements from function with left padding', function() {
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setTokenLength(0);
    suite.formatter.setFormatString('number of tasks: {4ly:numberoftasks}');
    let replacer = new TestReplacementProvider();
    suite.formatter.setReplacement('numberoftasks', function() {
        return replacer.getNumberOfTasks();
    });
    replacer.numberOfTasks = 3;
    Unit.assertEquals(suite.formatter.getString(), 'number of tasks: yyy3');
    replacer.numberOfTasks = 4;
    Unit.assertEquals(suite.formatter.getString(), 'number of tasks: yyy4');
});

suite.addTest('get string with replacements - printf style replacements with right padding', function() {
    suite.formatter.setReplacement('s', 'string');
    suite.formatter.setReplacement('d', 10);
    suite.formatter.setFormatString('%8rs %4r0d %3rs');
    Unit.assertEquals(suite.formatter.getString(), 'string   1000 string');
    suite.formatter.setFormatString('the string is %6rs and %1r0d is the decimal');
    Unit.assertEquals(suite.formatter.getString(), 'the string is string and 10 is the decimal');
});

suite.addTest('get string with replacements - bracket replacements with right padding', function() {
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setTokenLength(0);
    suite.formatter.setFormatString('{20r:somestring} {3rx:somenumber} {5r0:somestring}');
    suite.formatter.setReplacement('somestring', 'this is a string');
    suite.formatter.setReplacement('somenumber', 43);
    Unit.assertEquals(suite.formatter.getString(), 'this is a string     43x this is a string');
});

suite.addTest('get string with replacements - bracket replacements from function with right padding', function() {
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setTokenLength(0);
    suite.formatter.setFormatString('number of tasks: {4ry:numberoftasks}');
    let replacer = new TestReplacementProvider();
    suite.formatter.setReplacement('numberoftasks', function() {
        return replacer.getNumberOfTasks();
    });
    replacer.numberOfTasks = 3;
    Unit.assertEquals(suite.formatter.getString(), 'number of tasks: 3yyy');
    replacer.numberOfTasks = 4;
    Unit.assertEquals(suite.formatter.getString(), 'number of tasks: 4yyy');
});

suite.addTest('get string with replacements - printf style replacements with both sides padding with right emphasis',
    function() {
        suite.formatter.setReplacement('s', 'string');
        suite.formatter.setReplacement('d', 10);
        suite.formatter.setFormatString('%8Rs %5R0d %3Rs');
        Unit.assertEquals(suite.formatter.getString(), ' string  01000 string');
        suite.formatter.setFormatString('the string is %6Rs and %1R0d is the decimal');
        Unit.assertEquals(suite.formatter.getString(), 'the string is string and 10 is the decimal');
    });

suite.addTest('get string with replacements - bracket replacements with both sides padding with right emphasis',
    function() {
        suite.formatter.setLeftDelimiter('{');
        suite.formatter.setRightDelimiter('}');
        suite.formatter.setTokenLength(0);
        suite.formatter.setFormatString('{20R:somestring} {3Rx:somenumber} {5R0:somestring}');
        suite.formatter.setReplacement('somestring', 'this is a string');
        suite.formatter.setReplacement('somenumber', 43);
        Unit.assertEquals(suite.formatter.getString(), '  this is a string   43x this is a string');
    });

suite.addTest(
    'get string with replacements - bracket replacements from function with both sides padding with right emphasis',
    function() {
        suite.formatter.setLeftDelimiter('{');
        suite.formatter.setRightDelimiter('}');
        suite.formatter.setTokenLength(0);
        suite.formatter.setFormatString('number of tasks: {4Ry:numberoftasks}');
        let replacer = new TestReplacementProvider();
        suite.formatter.setReplacement('numberoftasks', function() {
            return replacer.getNumberOfTasks();
        });
        replacer.numberOfTasks = 3;
        Unit.assertEquals(suite.formatter.getString(), 'number of tasks: y3yy');
        replacer.numberOfTasks = 4;
        Unit.assertEquals(suite.formatter.getString(), 'number of tasks: y4yy');
    });

suite.addTest('get string with replacements - printf style replacements with both sides padding with left emphasis',
    function() {
        suite.formatter.setReplacement('s', 'string');
        suite.formatter.setReplacement('d', 10);
        suite.formatter.setFormatString('%8Ls %5L0d %3Ls');
        Unit.assertEquals(suite.formatter.getString(), ' string  00100 string');
        suite.formatter.setFormatString('the string is %6Ls and %1L0d is the decimal');
        Unit.assertEquals(suite.formatter.getString(), 'the string is string and 10 is the decimal');
    });

suite.addTest('get string with replacements - bracket replacements with both sides padding with left emphasis',
    function() {
        suite.formatter.setLeftDelimiter('{');
        suite.formatter.setRightDelimiter('}');
        suite.formatter.setTokenLength(0);
        suite.formatter.setFormatString('{20L:somestring} {3Lx:somenumber} {5L0:somestring}');
        suite.formatter.setReplacement('somestring', 'this is a string');
        suite.formatter.setReplacement('somenumber', 43);
        Unit.assertEquals(suite.formatter.getString(), '  this is a string   x43 this is a string');
    });

suite.addTest(
    'get string with replacements - bracket replacements from function with both sides padding with left emphasis',
    function() {
        suite.formatter.setLeftDelimiter('{');
        suite.formatter.setRightDelimiter('}');
        suite.formatter.setTokenLength(0);
        suite.formatter.setFormatString('number of tasks: {4Ly:numberoftasks}');
        let replacer = new TestReplacementProvider();
        suite.formatter.setReplacement('numberoftasks', function() {
            return replacer.getNumberOfTasks();
        });
        replacer.numberOfTasks = 3;
        Unit.assertEquals(suite.formatter.getString(), 'number of tasks: yy3y');
        replacer.numberOfTasks = 4;
        Unit.assertEquals(suite.formatter.getString(), 'number of tasks: yy4y');
    });

suite.addTest('get string with replacements - printf style replacements - pass formatstring', function() {
    suite.formatter.setReplacement('s', 'string');
    suite.formatter.setReplacement('d', 10);
    suite.formatter.setFormatString('%s %d');
    Unit.assertEquals(suite.formatter.getString(), 'string 10');
    Unit.assertEquals(suite.formatter.getString({
        formatString: '%d %s'
    }), '10 string');
    Unit.assertEquals(suite.formatter.getString(), 'string 10');
});

suite.addTest('get string with replacements - bracket replacements - pass formatstring', function() {
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setTokenLength(0);
    suite.formatter.setFormatString('{somestring} {somenumber} {somestring}');
    suite.formatter.setReplacement('somestring', 'this is a string');
    suite.formatter.setReplacement('somenumber', 43);
    Unit.assertEquals(suite.formatter.getString(), 'this is a string 43 this is a string');
    Unit.assertEquals(suite.formatter.getString({
        formatString: '{somenumber} {somestring}'
    }), '43 this is a string');
    Unit.assertEquals(suite.formatter.getString(), 'this is a string 43 this is a string');
});

suite.addTest('get string with replacements - bracket replacements from function', function() {
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setTokenLength(0);
    suite.formatter.setFormatString('number of tasks: {numberoftasks}');
    let replacer = new TestReplacementProvider();
    suite.formatter.setReplacement('numberoftasks', function() {
        return replacer.getNumberOfTasks();
    });
    replacer.numberOfTasks = 3;
    Unit.assertEquals(suite.formatter.getString(), 'number of tasks: 3');
    Unit.assertEquals(suite.formatter.getString({
            formatString: '{numberoftasks} is the number of tasks'
        }),
        '3 is the number of tasks');
    replacer.numberOfTasks = 4;
    Unit.assertEquals(suite.formatter.getString(), 'number of tasks: 4');
    Unit.assertEquals(suite.formatter.getString({
            formatString: '{numberoftasks} is the number of tasks'
        }),
        '4 is the number of tasks');
});

suite.addTest('get string with replacements - bracket replacement with overrides', function() {
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setTokenLength(0);
    suite.formatter.setFormatString('a number: {anumber}, a string: {astring}');
    suite.formatter.setReplacement('anumber', 3);
    suite.formatter.setReplacement('astring', 'three');
    Unit.assertEquals(suite.formatter.getString(), 'a number: 3, a string: three');
    Unit.assertEquals(suite.formatter.getString({
        formatString: 'is {astring} actually {anumber}?'
    }), 'is three actually 3?');
    Unit.assertEquals(suite.formatter.getString({
        overrideReplacements: {
            'astring': 'four',
            'anumber': 4
        }
    }), 'a number: 4, a string: four');
    Unit.assertEquals(suite.formatter.getString({
        formatString: 'is {astring} actually {anumber}?',
        overrideReplacements: {
            'astring': 'four',
            'anumber': 4
        }
    }), 'is four actually 4?');
    Unit.assertEquals(suite.formatter.getString(), 'a number: 3, a string: three');
});

suite.addTest('set external parser delimiters', function() {
    suite.formatter.setExternalParserLeftDelimiter('[');
    suite.formatter.setExternalParserRightDelimiter(']');
    Unit.assertEquals(suite.formatter._externalParserLeftDelimiter, '[');
    Unit.assertEquals(suite.formatter._externalParserRightDelimiter, ']');
    suite.formatter.setExternalParserLeftDelimiter();
    suite.formatter.setExternalParserRightDelimiter();
    Unit.assertEquals(suite.formatter._externalParserLeftDelimiter, '|');
    Unit.assertEquals(suite.formatter._externalParserRightDelimiter, '|');
});

suite.addTest('set external parser', function() {
    suite.formatter.setExternalParser(3);
    Unit.assertNull(suite.formatter._externalParser);
    let parser = function(string) {
        return string;
    };
    suite.formatter.setExternalParser(parser);
    Unit.assertEquals(suite.formatter._externalParser, parser);
    suite.formatter.setExternalParser(null);
    Unit.assertNull(suite.formatter._externalParser);
});

suite.addTest('get string with replacements - external parsing function', function() {
    suite.formatter.setExternalParser(function(string) {
        return string.toUpperCase();
    });
    suite.formatter.setFormatString('a number: %d, a string: |%s|');
    suite.formatter.setReplacement('d', 5);
    suite.formatter.setReplacement('s', 'lowercase');
    Unit.assertEquals(suite.formatter.getString(), 'a number: 5, a string: LOWERCASE');
    Unit.assertEquals(suite.formatter.getString({
        formatString: 'is uppercase really |%s|?',
        overrideReplacements: {
            's': 'uppercase'
        }
    }), 'is uppercase really UPPERCASE?');
});

suite.addTest('get string with replacements - parse multiple elements', function() {
    suite.formatter.setExternalParser(function(string) {
        return string.toUpperCase();
    });
    suite.formatter.setLeftDelimiter('{');
    suite.formatter.setRightDelimiter('}');
    suite.formatter.setTokenLength(0);
    suite.formatter.setFormatString('{stringone} |{stringtwo}||{stringthree}| {number} |{stringfour}');
    suite.formatter.setReplacement('stringone', 'first');
    suite.formatter.setReplacement('stringtwo', 'second');
    suite.formatter.setReplacement('stringthree', 'third');
    suite.formatter.setReplacement('stringfour', 'fourth');
    suite.formatter.setReplacement('number', 41);
    Unit.assertEquals(suite.formatter.getString(), 'first SECONDTHIRD 41 |fourth');
});
