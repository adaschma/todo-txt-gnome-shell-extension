const Extension = imports.misc.extensionUtils.getCurrentExtension();
const FileUtils = imports.unit.lib.fileUtils;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Helper = imports.unit.lib.utest_helpers;
const JsTextFile = imports.jsTextFile;
const Params = imports.misc.params;
const Settings = imports.settings.Settings;
const Unit = imports.third_party.gjsunit.gjsunit;

var suite = new Unit.Suite('Settings - error cases');

suite.addTest('init with non-existing path', function() {
    let message = Helper.assertThrows(function() {
        let params = {
            settingsFile: '/bla/this/bla/path/bla/does/bla/not/bla/exist/bla/bla'
        };
        new Settings(params);
    }, 'IoError');
    Unit.assertEquals(
        'JsTextFile: trying to load non-existing file /bla/this/bla/path/bla/does/bla/not/bla/exist/bla/bla',
        message);
});

suite.addTest('init with invalid json file', function() {
    let invalidSettingsJSON = Helper.makeTemporaryFile('[{foo:bar},{"foo":"bar"}]');
    let message = Helper.assertThrows(function() {
        let params = {
            settingsFile: invalidSettingsJSON.get_path()
        };
        new Settings(params);
    }, 'JsonError');
    Unit.assertEquals(invalidSettingsJSON.get_path() + ' is not a valid JSON file', message);
    FileUtils.deleteGFile(invalidSettingsJSON);
});

var validSuite = new Unit.Suite('Settings - valid cases');

validSuite.setup = function() {
    validSuite._settingsJSON = GLib.build_filenamev([Extension.path, 'unit', 'resources', 'test.json']);
    validSuite._xmlpath = GLib.build_filenamev([Extension.path, 'unit', 'resources', 'schemas']);
    GLib.spawn_command_line_sync('glib-compile-schemas ' + validSuite._xmlpath);
    let parameters = {
        settingsFile: validSuite._settingsJSON,
        schema: 'org.gnome.shell.extensions.TodoTxt.unit',
        schemaDir: validSuite._xmlpath
    };
    validSuite.settings = new Settings(parameters);
};

validSuite.teardown = function() {
    for (var key in validSuite.settings.getGioSettings().list_keys()) {
        if (validSuite.settings.getGioSettings().list_keys().hasOwnProperty(key)) {
            validSuite.settings.getGioSettings().reset(validSuite.settings.getGioSettings().list_keys()[key]);
        }
    }
    let compiled = Gio.file_new_for_path(GLib.build_filenamev([validSuite._xmlpath, 'gschemas.compiled']));
    FileUtils.deleteGFile(compiled);
    validSuite.settings = null;
};

validSuite.addTest('init', function() {
    Unit.assertEquals('boolean', validSuite.settings._loadedSettings.category1.sub1.angua.type);
    Unit.assertEquals('string', validSuite.settings._loadedSettings.category1.sub2.carrot.type);
    Unit.assertEquals('path', validSuite.settings._loadedSettings.category1.nobby.type);
    Unit.assertEquals('integer', validSuite.settings._loadedSettings.category2.sub1.cheery.type);
    Unit.assertEquals('dictionary', validSuite.settings._loadedSettings.category2.sub1.detritus.type);
    Unit.assertEquals('a{s(bsbb)}', validSuite.settings._loadedSettings.category2.sub1.detritus.signature);
    Unit.assertEquals('dictionary', validSuite.settings._loadedSettings.category2.sub2.dorfl.type);
    Unit.assertEquals('a{s(bsbb)}', validSuite.settings._loadedSettings.category2.sub2.dorfl.signature);
    Unit.assertEquals('boolean', validSuite.settings._loadedSettings.category3.sub1.plinge.type);
    Unit.assertEquals('integer', validSuite.settings._loadedSettings.category3.sub1.walter.type);
    Unit.assertEquals('shortcut', validSuite.settings._loadedSettings.category3.sub3.mazda.type);
});

validSuite.addTest('getType', function() {
    Unit.assertEquals('boolean', validSuite.settings.getType('angua'));
    Unit.assertEquals('string', validSuite.settings.getType('carrot'));
    Unit.assertEquals('path', validSuite.settings.getType('nobby'));
    Unit.assertEquals('integer', validSuite.settings.getType('cheery'));
    Unit.assertEquals('dictionary', validSuite.settings.getType('detritus'));
    Unit.assertEquals('dictionary', validSuite.settings.getType('dorfl'));
    Unit.assertEquals('boolean', validSuite.settings.getType('plinge'));
    Unit.assertEquals('integer', validSuite.settings.getType('walter'));
    Unit.assertEquals('shortcut', validSuite.settings.getType('mazda'));
    Unit.assertNull(validSuite.settings.getType('vimes'));
});

validSuite.addTest('getType - in category', function() {
    Unit.assertEquals('category', validSuite.settings.getType('category4'));
    Unit.assertEquals('subsection', validSuite.settings.getType('category4', 'category4'));
});

validSuite.addTest('exists', function() {
    Unit.assertTrue(validSuite.settings.exists('angua'));
    Unit.assertTrue(validSuite.settings.exists('carrot'));
    Unit.assertTrue(validSuite.settings.exists('nobby'));
    Unit.assertTrue(validSuite.settings.exists('cheery'));
    Unit.assertTrue(validSuite.settings.exists('detritus'));
    Unit.assertTrue(validSuite.settings.exists('dorfl'));
    Unit.assertTrue(validSuite.settings.exists('plinge'));
    Unit.assertTrue(validSuite.settings.exists('walter'));
    Unit.assertTrue(validSuite.settings.exists('mazda'));
    Unit.assertFalse(validSuite.settings.exists('vimes'));
});

validSuite.addTest('get boolean', function() {
    Unit.assertFalse(validSuite.settings.get('angua'));
});

validSuite.addTest('set boolean', function() {
    validSuite.settings.set('angua', true);
    Unit.assertTrue(validSuite.settings.get('angua'));
});

validSuite.addTest('get string', function() {
    Unit.assertEquals('dwarf', validSuite.settings.get('carrot'));
});

validSuite.addTest('set string', function() {
    validSuite.settings.set('carrot', 'huge dwarf');
    Unit.assertEquals('huge dwarf', validSuite.settings.get('carrot'));
});

validSuite.addTest('set string with wrong type', function() {
    let message = Helper.assertThrows(function() {
        validSuite.settings.set('carrot', true);
    }, 'SettingsTypeError');
    Unit.assertEquals('Expected value of type string, but got boolean while setting carrot', message);
    Unit.assertEquals('dwarf', validSuite.settings.get('carrot'));
});

validSuite.addTest('get path', function() {
    Unit.assertEquals(GLib.get_home_dir() + '/undefined/creature.txt', validSuite.settings.get('nobby'));
});

validSuite.addTest('set path', function() {
    validSuite.settings.set('nobby', GLib.get_home_dir() + '/front/desk.txt');
    Unit.assertEquals(GLib.get_home_dir() + '/front/desk.txt', validSuite.settings.get('nobby'));
});

validSuite.addTest('set path with wrong type', function() {
    let message = Helper.assertThrows(function() {
        validSuite.settings.set('nobby', true);
    }, 'SettingsTypeError');
    Unit.assertEquals('Expected value of type string, but got boolean while setting nobby', message);
    Unit.assertEquals(GLib.get_home_dir() + '/undefined/creature.txt', validSuite.settings.get('nobby'));
});

validSuite.addTest('get integer', function() {
    Unit.assertEquals(1, validSuite.settings.get('cheery'));
});

validSuite.addTest('set integer', function() {
    validSuite.settings.set('cheery', 2);
    Unit.assertEquals(2, validSuite.settings.get('cheery'));
});

validSuite.addTest('set integer with wrong type', function() {
    let message = Helper.assertThrows(function() {
        validSuite.settings.set('cheery', 'one');
    }, 'SettingsTypeError');
    Unit.assertEquals('Expected value of type integer, but got string while setting cheery', message);
    Unit.assertEquals(1, validSuite.settings.get('cheery'));
});


validSuite.addTest('get dictionary', function() {
    Unit.assertFalse(validSuite.settings.get('detritus').rock[0]);
    Unit.assertEquals('hard', validSuite.settings.get('detritus').rock[1]);
    Unit.assertTrue(validSuite.settings.get('detritus').rock[2]);
    Unit.assertFalse(validSuite.settings.get('detritus').rock[3]);
    Unit.assertTrue(validSuite.settings.get('detritus').pebble[0]);
    Unit.assertEquals('small', validSuite.settings.get('detritus').pebble[1]);
    Unit.assertFalse(validSuite.settings.get('detritus').pebble[2]);
    Unit.assertTrue(validSuite.settings.get('detritus').pebble[3]);
});

validSuite.addTest('set dictionary', function() {
    let dict = {};
    dict.rock = [true, 'soft', false, true];
    dict.pebble = [false, 'large', true, false];
    validSuite.settings.set('detritus', dict);
    Unit.assertTrue(validSuite.settings.get('detritus').rock[0]);
    Unit.assertEquals('soft', validSuite.settings.get('detritus').rock[1]);
    Unit.assertFalse(validSuite.settings.get('detritus').rock[2]);
    Unit.assertTrue(validSuite.settings.get('detritus').rock[3]);
    Unit.assertFalse(validSuite.settings.get('detritus').pebble[0]);
    Unit.assertEquals('large', validSuite.settings.get('detritus').pebble[1]);
    Unit.assertTrue(validSuite.settings.get('detritus').pebble[2]);
    Unit.assertFalse(validSuite.settings.get('detritus').pebble[3]);
});

validSuite.addTest('get non-existing', function() {
    Unit.assertNull(validSuite.settings.get('vimes'));
});

validSuite.addTest('get dictionary with value object', function() {
    let dorfl = validSuite.settings.get('dorfl');
    Unit.assertTrue(dorfl.golem.changeColor);
    Unit.assertTrue(dorfl.golem.bold);
    Unit.assertFalse(dorfl.golem.italic);
    Unit.assertEquals('rgb(12,12,12)', dorfl.golem.color.to_string());

    Unit.assertFalse(dorfl.feet.changeColor);
    Unit.assertTrue(dorfl.feet.bold);
    Unit.assertFalse(dorfl.feet.italic);
    Unit.assertEquals('rgb(65,14,0)', dorfl.feet.color.to_string());
});

validSuite.addTest('get shortcut', function() {
    Unit.assertEquals('<Super>f', validSuite.settings.get('mazda'));
});

validSuite.addTest('set shortcut', function() {
    validSuite.settings.set('mazda', '<Super>t');
    Unit.assertEquals('<Super>t', validSuite.settings.get('mazda'));
});

validSuite.addTest('get element existing in json, but not in schema, without default in json', function() {
    Unit.assertFalse(validSuite.settings.get('plinge'));
});

validSuite.addTest('get element existing in json, but not in schema, with default in json', function() {
    Unit.assertEquals(42, validSuite.settings.get('walter'));
});

validSuite.addTest('register for changes', function() {
    let dummyCalls = 0;
    let dummyCalls2 = 0;
    let dummyCallback = function() {
        dummyCalls++;
    };
    let dummyCallback2 = function() {
        dummyCalls2++;
    };
    validSuite.settings.registerForChange('angua', dummyCallback);
    validSuite.settings.set('angua', false);
    Unit.assertEquals(1, dummyCalls);
    validSuite.settings.set('angua', false);
    validSuite.settings.registerForChange('angua', dummyCallback2);
    Unit.assertEquals(2, dummyCalls);
    Unit.assertEquals(0, dummyCalls2);
    validSuite.settings.set('angua', true);
    Unit.assertEquals(3, dummyCalls);
    Unit.assertEquals(1, dummyCalls2);
});

validSuite.addTest('unregister callbacks', function() {
    let dummyCalls = 0;
    let dummyCalls2 = 0;
    let dummyCallback = function() {
        dummyCalls++;
    };
    let dummyCallback2 = function() {
        dummyCalls2++;
    };
    validSuite.settings.registerForChange('angua', dummyCallback);
    validSuite.settings.registerForChange('angua', dummyCallback2);
    validSuite.settings.set('angua', false);
    Unit.assertEquals(1, dummyCalls);
    Unit.assertEquals(1, dummyCalls2);
    validSuite.settings.unregisterCallbacks();
    validSuite.settings.set('angua', false);
    Unit.assertEquals(1, dummyCalls);
    Unit.assertEquals(1, dummyCalls2);
    validSuite.settings.registerForChange('angua', dummyCallback);
    validSuite.settings.registerForChange('angua', dummyCallback2);
    validSuite.settings.set('angua', false);
    Unit.assertEquals(2, dummyCalls);
    Unit.assertEquals(2, dummyCalls2);
    validSuite.settings.unregisterCallbacks();
});

validSuite.addTest('get category', function() {
    Unit.assertEquals('category1', validSuite.settings.getCategory('angua'));
    Unit.assertEquals('category1', validSuite.settings.getCategory('carrot'));
    Unit.assertEquals('category1', validSuite.settings.getCategory('nobby'));
    Unit.assertEquals('category2', validSuite.settings.getCategory('cheery'));
    Unit.assertEquals('category2', validSuite.settings.getCategory('detritus'));
    Unit.assertEquals('category2', validSuite.settings.getCategory('dorfl'));
    Unit.assertEquals('category3', validSuite.settings.getCategory('plinge'));
    Unit.assertEquals('category3', validSuite.settings.getCategory('walter'));
    Unit.assertEquals('category3', validSuite.settings.getCategory('mazda'));
    Unit.assertNull(validSuite.settings.getCategory('vimes'));
});

validSuite.addTest('get subcontainer', function() {
    Unit.assertEquals('sub1', validSuite.settings.getSubContainer('angua'));
    Unit.assertEquals('sub2', validSuite.settings.getSubContainer('carrot'));
    Unit.assertNull(validSuite.settings.getSubContainer('nobby'));
    Unit.assertEquals('sub1', validSuite.settings.getSubContainer('cheery'));
    Unit.assertEquals('sub1', validSuite.settings.getSubContainer('detritus'));
    Unit.assertEquals('sub2', validSuite.settings.getSubContainer('dorfl'));
    Unit.assertEquals('sub1', validSuite.settings.getSubContainer('plinge'));
    Unit.assertEquals('sub1', validSuite.settings.getSubContainer('walter'));
    Unit.assertEquals('sub3', validSuite.settings.getSubContainer('mazda'));
    Unit.assertNull(validSuite.settings.getCategory('vimes'));
});

validSuite.addTest('get all elements in category', function() {
    let all1 = validSuite.settings.getAllInCategory('category1');
    Unit.assertEquals(3, all1.length);
    Unit.assertArrayContainsElementThatMatches(all1, function(element) {
        return element[0] == 'angua';
    });
    Unit.assertArrayContainsElementThatMatches(all1, function(element) {
        return element[0] == 'carrot';
    });
    Unit.assertArrayContainsElementThatMatches(all1, function(element) {
        return element[0] == 'nobby';
    });
    let all2 = validSuite.settings.getAllInCategory('category2');
    Unit.assertEquals(3, all2.length);
    Unit.assertArrayContainsElementThatMatches(all2, function(element) {
        return element[0] == 'cheery';
    });
    Unit.assertArrayContainsElementThatMatches(all2, function(element) {
        return element[0] == 'detritus';
    });
    Unit.assertArrayContainsElementThatMatches(all2, function(element) {
        return element[0] == 'dorfl';
    });
    let all3 = validSuite.settings.getAllInCategory('category3');
    Unit.assertEquals(3, all3.length);
    Unit.assertArrayContainsElementThatMatches(all3, function(element) {
        return element[0] == 'plinge';
    });
    Unit.assertArrayContainsElementThatMatches(all3, function(element) {
        return element[0] == 'walter';
    });
    Unit.assertArrayContainsElementThatMatches(all3, function(element) {
        return element[0] == 'mazda';
    });
});

validSuite.addTest('get all elements in subcontainer', function() {
    let all11 = validSuite.settings.getAllInSubcontainer('category1', 'sub1');
    Unit.assertEquals(1, all11.length);
    Unit.assertArrayContainsElementThatMatches(all11, function(element) {
        return element[0] == 'angua';
    });
    let all12 = validSuite.settings.getAllInSubcontainer('category1', 'sub2');
    Unit.assertEquals(1, all12.length);
    Unit.assertArrayContainsElementThatMatches(all12, function(element) {
        return element[0] == 'carrot';
    });
    let all21 = validSuite.settings.getAllInSubcontainer('category2', 'sub1');
    Unit.assertEquals(2, all21.length);
    Unit.assertArrayContainsElementThatMatches(all21, function(element) {
        return element[0] == 'cheery';
    });
    Unit.assertArrayContainsElementThatMatches(all21, function(element) {
        return element[0] == 'detritus';
    });
    let all22 = validSuite.settings.getAllInSubcontainer('category2', 'sub2');
    Unit.assertEquals(1, all22.length);
    Unit.assertArrayContainsElementThatMatches(all22, function(element) {
        return element[0] == 'dorfl';
    });
    let all31 = validSuite.settings.getAllInSubcontainer('category3', 'sub1');
    Unit.assertEquals(2, all31.length);
    Unit.assertArrayContainsElementThatMatches(all31, function(element) {
        return element[0] == 'plinge';
    });
    Unit.assertArrayContainsElementThatMatches(all31, function(element) {
        return element[0] == 'walter';
    });
    let all33 = validSuite.settings.getAllInSubcontainer('category3', 'sub3');
    Unit.assertEquals(1, all33.length);
    Unit.assertArrayContainsElementThatMatches(all33, function(element) {
        return element[0] == 'mazda';
    });
});

validSuite.addTest('get all elements of a type', function() {
    let allBools = validSuite.settings.getAll('boolean');
    Unit.assertEquals(2, allBools.length);
    Unit.assertArrayContainsElementThatMatches(allBools, function(element) {
        return element[0] == 'angua';
    });
    Unit.assertArrayContainsElementThatMatches(allBools, function(element) {
        return element[0] == 'plinge';
    });
    let allStrings = validSuite.settings.getAll('string');
    Unit.assertEquals(1, allStrings.length);
    Unit.assertArrayContainsElementThatMatches(allStrings, function(element) {
        return element[0] == 'carrot';
    });
    let allPaths = validSuite.settings.getAll('path');
    Unit.assertEquals(1, allPaths.length);
    Unit.assertArrayContainsElementThatMatches(allPaths, function(element) {
        return element[0] == 'nobby';
    });
    let allIntegers = validSuite.settings.getAll('integer');
    Unit.assertEquals(2, allIntegers.length);
    Unit.assertArrayContainsElementThatMatches(allIntegers, function(element) {
        return element[0] == 'cheery';
    });
    Unit.assertArrayContainsElementThatMatches(allIntegers, function(element) {
        return element[0] == 'walter';
    });
    let allDictionarys = validSuite.settings.getAll('dictionary');
    Unit.assertEquals(2, allDictionarys.length);
    Unit.assertArrayContainsElementThatMatches(allDictionarys, function(element) {
        return element[0] == 'detritus';
    });
    Unit.assertArrayContainsElementThatMatches(allDictionarys, function(element) {
        return element[0] == 'dorfl';
    });
    let allShortcuts = validSuite.settings.getAll('shortcut');
    Unit.assertEquals(1, allShortcuts.length);
    Unit.assertArrayContainsElementThatMatches(allShortcuts, function(element) {
        return element[0] == 'mazda';
    });
});

validSuite.addTest('get all categories', function() {
    let allCategories = validSuite.settings.getAllCategories();
    Unit.assertEquals(4, allCategories.length);
    Unit.assertArrayEquals(allCategories, ['category1', 'category2', 'category3', 'category4']);
});

validSuite.addTest('get all subcontainers', function() {
    let cat1SubContainers = validSuite.settings.getAllSubContainers('category1');
    Unit.assertEquals(2, cat1SubContainers.length);
    Unit.assertArrayEquals(cat1SubContainers, ['sub1', 'sub2']);
    let cat2SubContainers = validSuite.settings.getAllSubContainers('category2');
    Unit.assertEquals(2, cat2SubContainers.length);
    Unit.assertArrayEquals(cat2SubContainers, ['sub1', 'sub2']);
    let cat3SubContainers = validSuite.settings.getAllSubContainers('category3');
    Unit.assertEquals(2, cat3SubContainers.length);
    Unit.assertArrayEquals(cat3SubContainers, ['sub1', 'sub3']);
});

validSuite.addTest('get summaries', function() {
    Unit.assertEquals('Werewolf', validSuite.settings.getSummary('angua'));
    Unit.assertEquals('Dwarf', validSuite.settings.getSummary('carrot'));
    Unit.assertEquals('Undefined path', validSuite.settings.getSummary('nobby'));
    Unit.assertEquals('Littlebottom', validSuite.settings.getSummary('cheery'));
    Unit.assertEquals('Hard Rock', validSuite.settings.getSummary('detritus'));
    Unit.assertEquals('Clay', validSuite.settings.getSummary('dorfl'));
    Unit.assertEquals('', validSuite.settings.getSummary('plinge'));
    Unit.assertEquals('', validSuite.settings.getSummary('walter'));
    Unit.assertEquals('fingers', validSuite.settings.getSummary('mazda'));
});

validSuite.addTest('get human names', function() {
    Unit.assertEquals('Angua', validSuite.settings.getHumanName('angua'));
    Unit.assertEquals('Carrot', validSuite.settings.getHumanName('carrot'));
    Unit.assertEquals('Nobby', validSuite.settings.getHumanName('nobby'));
    Unit.assertEquals('Cheery', validSuite.settings.getHumanName('cheery'));
    Unit.assertEquals('Detritus', validSuite.settings.getHumanName('detritus'));
    Unit.assertEquals('Dorfl', validSuite.settings.getHumanName('dorfl'));
    Unit.assertEquals('Plinge', validSuite.settings.getHumanName('plinge'));
    Unit.assertEquals('Walter', validSuite.settings.getHumanName('walter'));
    Unit.assertEquals('mazda', validSuite.settings.getHumanName('mazda'));
});

validSuite.addTest('get help', function() {
    Unit.assertEquals('', validSuite.settings.getHelp('angua'));
    Unit.assertEquals('', validSuite.settings.getHelp('carrot'));
    Unit.assertEquals('No help available', validSuite.settings.getHelp('nobby'));
    Unit.assertEquals('', validSuite.settings.getHelp('cheery'));
    Unit.assertEquals('', validSuite.settings.getHelp('detritus'));
    Unit.assertEquals('', validSuite.settings.getHelp('dorfl'));
    Unit.assertEquals('I need somebody', validSuite.settings.getHelp('plinge'));
    Unit.assertEquals('Heeeeeeelp', validSuite.settings.getHelp('walter'));
    Unit.assertEquals('', validSuite.settings.getHelp('mazda'));
});

validSuite.addTest('get extra_params', function() {
    Unit.assertNotNull(validSuite.settings.getExtraParams('angua'));
    let params = Params.parse(validSuite.settings.getExtraParams('angua'), {
        changing: 'Yes please',
        loving: 'Maybe'
    });
    Unit.assertEquals(params.changing, 'not currently');
    Unit.assertEquals(params.loving, 'Maybe');
    Unit.assertNull(validSuite.settings.getExtraParams('carrot'));
    Unit.assertNull(validSuite.settings.getExtraParams('nobby'));
    Unit.assertNull(validSuite.settings.getExtraParams('cheery'));
    Unit.assertNull(validSuite.settings.getExtraParams('detritus'));
    Unit.assertNull(validSuite.settings.getExtraParams('dorfl'));
    Unit.assertNull(validSuite.settings.getExtraParams('plinge'));
    Unit.assertNull(validSuite.settings.getExtraParams('walter'));
    Unit.assertNull(validSuite.settings.getExtraParams('mazda'));
});

validSuite.addTest('get widget', function() {
    Unit.assertEquals('default', validSuite.settings.getWidget('angua'));
    Unit.assertEquals('default', validSuite.settings.getWidget('carrot'));
    Unit.assertEquals('default', validSuite.settings.getWidget('nobby'));
    Unit.assertEquals('default', validSuite.settings.getWidget('cheery'));
    Unit.assertEquals('default', validSuite.settings.getWidget('detritus'));
    Unit.assertEquals('default', validSuite.settings.getWidget('dorfl'));
    Unit.assertEquals('none', validSuite.settings.getWidget('plinge'));
    Unit.assertEquals('default', validSuite.settings.getWidget('walter'));
    Unit.assertEquals('default', validSuite.settings.getWidget('mazda'));
});

validSuite.addTest('has subcategory', function() {
    Unit.assertTrue(validSuite.settings.hasSubcategories('category1'));
    Unit.assertTrue(validSuite.settings.hasSubcategories('category2'));
    Unit.assertFalse(validSuite.settings.hasSubcategories('category3'));
});

validSuite.addTest('get level', function() {
    Unit.assertEquals('0', validSuite.settings.getLevel('angua'));
    Unit.assertEquals('100', validSuite.settings.getLevel('carrot'));
    Unit.assertEquals('250', validSuite.settings.getLevel('nobby'));
    Unit.assertEquals('0', validSuite.settings.getLevel('cheery'));
    Unit.assertEquals('100', validSuite.settings.getLevel('detritus'));
    Unit.assertEquals('0', validSuite.settings.getLevel('dorfl'));
    Unit.assertEquals('100', validSuite.settings.getLevel('plinge'));
    Unit.assertEquals('250', validSuite.settings.getLevel('walter'));
    Unit.assertEquals('100', validSuite.settings.getLevel('mazda'));
});

/* vi: set expandtab tabstop=4 shiftwidth=4: */
