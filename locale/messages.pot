# todo.txt gnome-shell extension translation file
# This file is distributed under the same license as the todo.txt gnome-shell extension package.
# Translators:
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: todo.txt gnome-shell extension\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-04-13 15:58+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../errors.js:34
msgid "%(file) cannot be written. Please check its permissions"
msgstr ""

#: ../errors.js:45
msgid "An error occured while writing to %(file): %(error)"
msgstr ""

#: ../extension.js:137
msgid "New task..."
msgstr ""

#: ../extension.js:323
msgid "Open todo.txt file in text editor"
msgstr ""

#: ../extension.js:330
msgid "Cannot open file"
msgstr ""

#: ../extension.js:332
msgid "An error occured while trying to launch the default text editor: %(error)"
msgstr ""

#: ../extension.js:417
msgid "Ungrouped"
msgstr ""

#: ../extension.js:538
msgid "Use existing file"
msgstr ""

#: ../extension.js:543
msgid "Create new file"
msgstr ""

#: ../extension.js:549
msgid "Open settings"
msgstr ""

#: ../extension.js:553
msgid "Cancel"
msgstr ""

#: ../extension.js:556
msgid "%(file) exists already"
msgstr ""

#: ../extension.js:557
msgid "Please choose what you want to do"
msgstr ""

#: ../extension.js:575
msgid "No valid %(filename) file specified"
msgstr ""

#: ../extension.js:576
msgid "Select location in settings"
msgstr ""

#: ../extension.js:581
msgid "Create todo.txt and done.txt file in %(path)"
msgstr ""

#: ../extension.js:837 ../extension.js:895
msgid "Error writing file"
msgstr ""

#: ../extension.js:848 ../extension.js:902
msgid "Unknown error during file write: %(error)"
msgstr ""

#: ../messageDialog.js:40
msgid "Ok"
msgstr ""

#: ../todoMenuItem.js:175
msgid "Edit %(task)"
msgstr ""

#: ../todoMenuItem.js:185
msgid "Increase %(task) priority"
msgstr ""

#: ../todoMenuItem.js:186
msgid "Decrease %(task) priority"
msgstr ""

#: ../todoMenuItem.js:201
msgid "Archive %(task)"
msgstr ""

#: ../todoMenuItem.js:211 ../todoMenuItem.js:457
msgid "Delete %(task)"
msgstr ""

#: ../todoMenuItem.js:232
msgid "Expand %(task)"
msgstr ""

#: ../todoMenuItem.js:246
msgid "Mark %(task) as done"
msgstr ""

#: ../todoMenuItem.js:479
msgid "Undo delete %(task)"
msgstr ""

#: ../todoMenuItem.js:489
msgid "Are you sure?"
msgstr ""

#: ../preferences/pathWidget.js:16
msgid "Select file"
msgstr ""

#: ../preferences/pathWidget.js:49
msgid "Browse"
msgstr ""

#: ../preferences/preferenceWidget.js:124
msgid "No description"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:93
msgid "Wrong priority"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:107
msgid "Duplicate priority: %(priority)"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:206
msgid "Priority"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:230
msgid "Change color"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:233
msgid "Color"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:267
msgid "Bold"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:268
msgid "Italic"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:279
msgid "Add style"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:286
msgid "Please enter the priority"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:288
msgid "New priority style"
msgstr ""

#: ../preferences/priorityMarkupWidget.js:309
msgid "Delete"
msgstr ""

#: ../preferences/shortcutWidget.js:36
msgid "Function"
msgstr ""

#: ../preferences/shortcutWidget.js:78
msgid "Key"
msgstr ""

#: ../preferences/subCategoryTab.js:50
msgid "Help"
msgstr ""
# settings.json
msgid "Location of the text file that contains completed (archived) tasks"
msgstr ""

# settings.json
msgid "Auto-add creation date to new tasks"
msgstr ""

# settings.json
msgid "Expert"
msgstr ""

# settings.json
msgid "Whether a button is shown to edit a task"
msgstr ""

# settings.json
msgid "Open task list"
msgstr ""

# settings.json
msgid "Bottom of the file"
msgstr ""

# settings.json
msgid "Keep open menu after adding new task"
msgstr ""

# settings.json
msgid "Top Bar"
msgstr ""

# settings.json
msgid "Files"
msgstr ""

# settings.json
msgid ""
"Templates can contain the following patterns: \n"
"\t{undone}: number of tasks that are not completed yet\n"
"\t{unarchived}: number of tasks that are not archived yet\n"
"\n"
"If you surround an expression with pipe characters (|), the pattern will be mathematically evaluated after the replacements have been done.\n"
"For example: '{unarchived}-{undone}' will render as '3-2' for 3 unarchived and 2 undone tasks, but '|{unarchived}-{undone}| will render as '1'\n"
"\n"
"\n"
"You can also use a prefix to pad a number.\n"
"The prefix consists of three elements:\n"
"\tA number indicating the desired width. If the number is wider, no padding will be done\n"
"\tA letter indicating the padding direction:\n"
"\t\tl: Pad left\n"
"\t\tr: Pad right\n"
"\t\tL: Pad at both sides, but more at left side if uneven padding\n"
"\t\tR: Pad at both sides, but more at right side if uneven padding\n"
"\tThe character to pad with (optional, default is a space)\n"
"\tA ':' to split the prefix and the wildcard\n"
"\n"
"Examples:\n"
"\t{3lx:undone} will render as xx2 for 2 undone tasks\n"
"\t{4R0:unarchived} will render as 0300 if there are 3 unarchived tasks"
msgstr ""

# settings.json
msgid "Hide if pattern is zero"
msgstr ""

# settings.json
msgid "Where in the text-file the new task will be added. Also moves insert box to the top of the interface."
msgstr ""

# settings.json
msgid "Scroll up/down to contract/expand"
msgstr ""

# settings.json
msgid "Icon"
msgstr ""

# settings.json
msgid "Put ungrouped tasks in separate group"
msgstr ""

# settings.json
msgid "Both"
msgstr ""

# settings.json
msgid "Mark task as done or archive task"
msgstr ""

# settings.json
msgid "Whether projects are shown in the interface (does not affect grouping)"
msgstr ""

# settings.json
msgid "Extensions"
msgstr ""

# settings.json
msgid "Debug"
msgstr ""

# settings.json
msgid "Display"
msgstr ""

# settings.json
msgid "Show projects"
msgstr ""

# settings.json
msgid "Level of settings that is shown"
msgstr ""

# settings.json
msgid "Pattern to match for zero"
msgstr ""

# settings.json
msgid "Select location of done.txt file"
msgstr ""

# settings.json
msgid "Truncate long tasks"
msgstr ""

# settings.json
msgid "This color will be used for URLs if 'Custom color' was selected above"
msgstr ""

# settings.json
msgid "Warning"
msgstr ""

# settings.json
msgid "Advanced"
msgstr ""

# settings.json
msgid "Same as task"
msgstr ""

# settings.json
msgid "Interface elements"
msgstr ""

# settings.json
msgid "Show change task priority buttons"
msgstr ""

# settings.json
msgid "What to do when a task is clicked"
msgstr ""

# settings.json
msgid "Info"
msgstr ""

# settings.json
msgid "Settings level"
msgstr ""

# settings.json
msgid "End"
msgstr ""

# settings.json
msgid "Style priorities"
msgstr ""

# settings.json
msgid "Whether arrows are shown to increase or decrease the task priority"
msgstr ""

# settings.json
msgid "The way that tasks with different priorities are displayed"
msgstr ""

# settings.json
msgid "Location to insert new task"
msgstr ""

# settings.json
msgid "If the hidden extension is enabled, tasks containing 'h:1' will not be shown"
msgstr ""

# settings.json
msgid "Location of the text file that contains the tasks in todo.txt syntax"
msgstr ""

# settings.json
msgid "Whether long tasks are truncated if they exceed a specified width"
msgstr ""

# settings.json
msgid "Styles"
msgstr ""

# settings.json
msgid "Whether contexts are shown in the interface (does not affect grouping)"
msgstr ""

# settings.json
msgid "Template string for display"
msgstr ""

# settings.json
msgid "Nothing"
msgstr ""

# settings.json
msgid "Action on clicking task"
msgstr ""

# settings.json
msgid "Projects"
msgstr ""

# settings.json
msgid "Tasks will be truncated to this width (specified in pixels) if truncating is enabled"
msgstr ""

# settings.json
msgid "Select location of todo.txt file"
msgstr ""

# settings.json
msgid "Priority on task completion"
msgstr ""

# settings.json
msgid "Whether a menu element is shown to open the todo.txt file in the default text editor"
msgstr ""

# settings.json
msgid "Behavior"
msgstr ""

# settings.json
msgid "Method to expand/contract truncated tasks"
msgstr ""

# settings.json
msgid "Grouping"
msgstr ""

# settings.json
msgid "Debug level"
msgstr ""

# settings.json
msgid "Custom color for URLS"
msgstr ""

# settings.json
msgid "Truncating"
msgstr ""

# settings.json
msgid "Show done/archive task button"
msgstr ""

# settings.json
msgid ""
"\n"
"\n"
"Note that padding and | cannot be used for the zero-matching pattern"
msgstr ""

# settings.json
msgid "Keeps open the tasks menu after a new task is added. The new task entry will be focused"
msgstr ""

# settings.json
msgid "Whether tasks with a certain priority are shown in a specific style"
msgstr ""

# settings.json
msgid "Detail"
msgstr ""

# settings.json
msgid "Start"
msgstr ""

# settings.json
msgid "Whether a creation date is automatically added to newly created tasks"
msgstr ""

# settings.json
msgid "Whether a confirmation dialog is shown before deleting a task"
msgstr ""

# settings.json
msgid "Basic"
msgstr ""

# settings.json
msgid "Priority markup"
msgstr ""

# settings.json
msgid "Confirm task deletion"
msgstr ""

# settings.json
msgid "Whether a button is shown to mark active tasks as completed or to archive completed tasks, if auto-archive is off"
msgstr ""

# settings.json
msgid "Keep with pri: prefix"
msgstr ""

# settings.json
msgid "Top of the file"
msgstr ""

# settings.json
msgid "Keep as is (non-standard)"
msgstr ""

# settings.json
msgid "Custom color"
msgstr ""

# settings.json
msgid "When URLs are detected in a task, they will be displayed in this color"
msgstr ""

# settings.json
msgid "Levels"
msgstr ""

# settings.json
msgid "No grouping"
msgstr ""

# settings.json
msgid "If this is true, an icon will be shown in the top bar"
msgstr ""

# settings.json
msgid "Hidden tasks extension"
msgstr ""

# settings.json
msgid "Todo.txt location"
msgstr ""

# settings.json
msgid "Tasks that don't have the grouping priority can be put in a special 'ungrouped' group, or shown outside any groups"
msgstr ""

# settings.json
msgid "Show number of tasks in group"
msgstr ""

# settings.json
msgid "Color for detected URLs"
msgstr ""

# settings.json
msgid "Flow"
msgstr ""

# settings.json
msgid "Template that determines what is displayed in the top bar"
msgstr ""

# settings.json
msgid "Priorities"
msgstr ""

# settings.json
msgid "Auto-archive done tasks"
msgstr ""

# settings.json
msgid "The number of tasks in a subgroup can be shown in the interface"
msgstr ""

# settings.json
msgid "Error"
msgstr ""

# settings.json
msgid "Maximum task width in pixels"
msgstr ""

# settings.json
msgid "Edit task"
msgstr ""

# settings.json
msgid "Shortcuts"
msgstr ""

# settings.json
msgid "Text"
msgstr ""

# settings.json
msgid "General"
msgstr ""

# settings.json
msgid "Location to truncate long tasks"
msgstr ""

# settings.json
msgid "Show icon"
msgstr ""

# settings.json
msgid "Show delete task button"
msgstr ""

# settings.json
msgid "Show new task entry"
msgstr ""

# settings.json
msgid "Level of debug information"
msgstr ""

# settings.json
msgid "The location in the task text where the ellipsization will occur"
msgstr ""

# settings.json
msgid "Show 'open in text editor'"
msgstr ""

# settings.json
msgid "Done.txt location"
msgstr ""

# settings.json
msgid "Whether an entry field is shown to create new tasks (new tasks can also be added by modifying the todo.txt file)"
msgstr ""

# settings.json
msgid "Tasks can be grouped together based on the selected property"
msgstr ""

# settings.json
msgid "Whether completed tasks will be automatically archived (i.e. moved to the done.txt file)"
msgstr ""

# settings.json
msgid ""
"\n"
"\n"
"When using the 'hidden' extension, an extra pattern is available:\n"
"\t{hidden}: number of hidden tasks"
msgstr ""

# settings.json
msgid "Whether a button is shown to delete a task"
msgstr ""

# settings.json
msgid "What should be done with the priority of a task when that task is completed"
msgstr ""

# settings.json
msgid "Show edit task button"
msgstr ""

# settings.json
msgid "Group tasks by"
msgstr ""

# settings.json
msgid "Contexts"
msgstr ""

# settings.json
msgid "If the specified pattern is zero, the elements specified here will be hidden"
msgstr ""

# settings.json
msgid "Show contexts"
msgstr ""

# settings.json
msgid "Remove"
msgstr ""

# settings.json
msgid "Dedicated button"
msgstr ""

# settings.json
msgid "URL Color"
msgstr ""

# settings.json
msgid "Middle"
msgstr ""

# settings.json
msgid "The action that will initiate expansion and contraction of tasks"
msgstr ""

# settings.json
msgid "If this template evaluates to zero, the top bar element will be hidden. Shortcuts still work."
msgstr ""

# settings.json
msgid "Get color from theme"
msgstr ""

